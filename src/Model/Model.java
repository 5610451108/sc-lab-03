package Model;

public class Model {
	
	private String name;

	public int changeAscii(String result){
		int sumascii = 0;
		for(char c: result.toCharArray()){
			sumascii  = sumascii + (int) c;
		}		
		return sumascii;	
	}
	
	public int modAscii(int ascii){
		int sum = 0;
		sum = sum + (ascii%4);
		return sum;
		
	}
	
	public void setName(String str){
		name = str;		
	}
	
	public String toString(){
		return name;
	}
}
	
