package Control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import Model.Model;
import View.View;

public class Controller {

	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);

		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Controller();
	}

	public Controller() {
		frame = new View();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(400, 400);
		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();
	}

	public void setTestCase() {
		String url1 = "www.ragnarok.com";
		Model M1 = new Model();
		M1.setName(url1);
		int num1 = M1.changeAscii(url1);
		int ascii1 = M1.modAscii(num1);
		frame.setResult("1. " + M1.toString()+ " : " + num1 + " = "+ ascii1);
	
		String url2 = "www.kapook.com";
		Model M2 = new Model();
		M2.setName(url2);
		int num2 = M2.changeAscii(url2);
		int ascii2 = M2.modAscii(num2);
		frame.extendResult("2. " + M2.toString()+ " : " + num2 + " = "+ ascii2);
	
		String url3 = "www.only3.com";
		Model M3 = new Model();
		M3.setName(url3);
		int num3 = M3.changeAscii(url3);
		int ascii3 = M3.modAscii(num3);
		frame.extendResult("3. " + M3.toString()+ " : " + num3 + " = "+ ascii3);
	
		String url4 = "www.abb.com ";
		Model M4 = new Model();
		M4.setName(url4);
		int num4 = M4.changeAscii(url4);
		int ascii4 = M4.modAscii(num4);
		frame.extendResult("4. " + M4.toString()+ " : " + num4 + " = "+ ascii4);
		
		String url5 = "www.phantasyy.com";
		Model M5 = new Model();
		M5.setName(url5);
		int num5 = M5.changeAscii(url5);
		int ascii5 = M5.modAscii(num5);
		frame.extendResult("5. " + M5.toString()+ " : " + num5 + " = "+ ascii5);
		
		String url6 = "www.nuthum.com";
		Model M6 = new Model();
		M6.setName(url6);
		int num6 = M6.changeAscii(url6);
		int ascii6 = M6.modAscii(num6);
		frame.extendResult("6. " + M6.toString()+ " : " + num6 + " = "+ ascii6);
		
		String url7 = "www.nuchdeb.com";
		Model M7 = new Model();
		M7.setName(url7);
		int num7 = M7.changeAscii(url7);
		int ascii7 = M7.modAscii(num7);
		frame.extendResult("7. " + M7.toString()+ " : " + num7 + " = "+ ascii7);
		
		String url8 = "www.jvonline.com";
		Model M8 = new Model();
		M8.setName(url8);
		int num8 = M8.changeAscii(url8);
		int ascii8 = M8.modAscii(num8);
		frame.extendResult("8. " + M8.toString()+ " : " + num8 + " = "+ ascii8);
		
		String url9 = "www.cakeroll.com";
		Model M9 = new Model();
		M9.setName(url9);
		int num9 = M9.changeAscii(url9);
		int ascii9 = M9.modAscii(num9);
		frame.extendResult("9. " + M9.toString()+ " : " + num9 + " = "+ ascii9);
		
		String url10 = "www.darius.co.th";
		Model M10 = new Model();
		M10.setName(url10);
		int num10 = M10.changeAscii(url10);
		int ascii10 = M10.modAscii(num10);
		frame.extendResult("10. " + M10.toString()+ " : " + num10 + " = "+ ascii10);
		
		String url11 = "www.resone.co.th";
		Model M11 = new Model();
		M11.setName(url11);
		int num11 = M11.changeAscii(url11);
		int ascii11 = M11.modAscii(num11);
		frame.extendResult("11. " + M11.toString()+ " : " + num11 + " = "+ ascii11);
		
		String url12 = "www.nendo.co.th";
		Model M12 = new Model();
		M12.setName(url12);
		int num12 = M12.changeAscii(url12);
		int ascii12 = M12.modAscii(num12);
		frame.extendResult("12. " + M12.toString()+ " : " + num12 + " = "+ ascii12);
		
		
		
		
	}

	ActionListener list;
	View frame;
}
